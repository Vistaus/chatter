/****************************************************************************
** Meta object code from reading C++ file 'irc_client.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "irc_client.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'irc_client.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_IRCUser_t {
    QByteArrayData data[12];
    char stringdata[144];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_IRCUser_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_IRCUser_t qt_meta_stringdata_IRCUser = {
    {
QT_MOC_LITERAL(0, 0, 7), // "IRCUser"
QT_MOC_LITERAL(1, 8, 15), // "nicknameChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 15), // "usernameChanged"
QT_MOC_LITERAL(4, 41, 15), // "hostnameChanged"
QT_MOC_LITERAL(5, 57, 15), // "realNameChanged"
QT_MOC_LITERAL(6, 73, 20), // "clientVersionChanged"
QT_MOC_LITERAL(7, 94, 8), // "nickname"
QT_MOC_LITERAL(8, 103, 8), // "username"
QT_MOC_LITERAL(9, 112, 8), // "hostname"
QT_MOC_LITERAL(10, 121, 8), // "realName"
QT_MOC_LITERAL(11, 130, 13) // "clientVersion"

    },
    "IRCUser\0nicknameChanged\0\0usernameChanged\0"
    "hostnameChanged\0realNameChanged\0"
    "clientVersionChanged\0nickname\0username\0"
    "hostname\0realName\0clientVersion"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_IRCUser[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       5,   44, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,
       3,    0,   40,    2, 0x06 /* Public */,
       4,    0,   41,    2, 0x06 /* Public */,
       5,    0,   42,    2, 0x06 /* Public */,
       6,    0,   43,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       7, QMetaType::QString, 0x00495103,
       8, QMetaType::QString, 0x00495103,
       9, QMetaType::QString, 0x00495103,
      10, QMetaType::QString, 0x00495103,
      11, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void IRCUser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        IRCUser *_t = static_cast<IRCUser *>(_o);
        switch (_id) {
        case 0: _t->nicknameChanged(); break;
        case 1: _t->usernameChanged(); break;
        case 2: _t->hostnameChanged(); break;
        case 3: _t->realNameChanged(); break;
        case 4: _t->clientVersionChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (IRCUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCUser::nicknameChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (IRCUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCUser::usernameChanged)) {
                *result = 1;
            }
        }
        {
            typedef void (IRCUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCUser::hostnameChanged)) {
                *result = 2;
            }
        }
        {
            typedef void (IRCUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCUser::realNameChanged)) {
                *result = 3;
            }
        }
        {
            typedef void (IRCUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCUser::clientVersionChanged)) {
                *result = 4;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject IRCUser::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_IRCUser.data,
      qt_meta_data_IRCUser,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *IRCUser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *IRCUser::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_IRCUser.stringdata))
        return static_cast<void*>(const_cast< IRCUser*>(this));
    return QObject::qt_metacast(_clname);
}

int IRCUser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = nickname(); break;
        case 1: *reinterpret_cast< QString*>(_v) = username(); break;
        case 2: *reinterpret_cast< QString*>(_v) = hostname(); break;
        case 3: *reinterpret_cast< QString*>(_v) = realName(); break;
        case 4: *reinterpret_cast< QString*>(_v) = clientVersion(); break;
        default: break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setNickname(*reinterpret_cast< QString*>(_v)); break;
        case 1: setUsername(*reinterpret_cast< QString*>(_v)); break;
        case 2: setHostname(*reinterpret_cast< QString*>(_v)); break;
        case 3: setRealName(*reinterpret_cast< QString*>(_v)); break;
        case 4: setClientVersion(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void IRCUser::nicknameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void IRCUser::usernameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void IRCUser::hostnameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void IRCUser::realNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void IRCUser::clientVersionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}
struct qt_meta_stringdata_IRCClient_t {
    QByteArrayData data[110];
    char stringdata[1380];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_IRCClient_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_IRCClient_t qt_meta_stringdata_IRCClient = {
    {
QT_MOC_LITERAL(0, 0, 9), // "IRCClient"
QT_MOC_LITERAL(1, 10, 15), // "commandReceived"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 6), // "prefix"
QT_MOC_LITERAL(4, 34, 7), // "command"
QT_MOC_LITERAL(5, 42, 6), // "params"
QT_MOC_LITERAL(6, 49, 11), // "commandSent"
QT_MOC_LITERAL(7, 61, 15), // "welcomeReceived"
QT_MOC_LITERAL(8, 77, 8), // "nickname"
QT_MOC_LITERAL(9, 86, 7), // "message"
QT_MOC_LITERAL(10, 94, 23), // "nicknameChangedReceived"
QT_MOC_LITERAL(11, 118, 12), // "joinReceived"
QT_MOC_LITERAL(12, 131, 7), // "channel"
QT_MOC_LITERAL(13, 139, 12), // "quitReceived"
QT_MOC_LITERAL(14, 152, 12), // "partReceived"
QT_MOC_LITERAL(15, 165, 16), // "setTopicReceived"
QT_MOC_LITERAL(16, 182, 5), // "topic"
QT_MOC_LITERAL(17, 188, 15), // "messageReceived"
QT_MOC_LITERAL(18, 204, 6), // "target"
QT_MOC_LITERAL(19, 211, 4), // "text"
QT_MOC_LITERAL(20, 216, 12), // "canAutoReply"
QT_MOC_LITERAL(21, 229, 12), // "killReceived"
QT_MOC_LITERAL(22, 242, 7), // "comment"
QT_MOC_LITERAL(23, 250, 12), // "pingReceived"
QT_MOC_LITERAL(24, 263, 7), // "server1"
QT_MOC_LITERAL(25, 271, 7), // "server2"
QT_MOC_LITERAL(26, 279, 12), // "pongReceived"
QT_MOC_LITERAL(27, 292, 13), // "errorReceived"
QT_MOC_LITERAL(28, 306, 16), // "userInfoReceived"
QT_MOC_LITERAL(29, 323, 8), // "username"
QT_MOC_LITERAL(30, 332, 8), // "hostname"
QT_MOC_LITERAL(31, 341, 8), // "realName"
QT_MOC_LITERAL(32, 350, 21), // "getTopicReplyReceived"
QT_MOC_LITERAL(33, 372, 21), // "getNamesReplyReceived"
QT_MOC_LITERAL(34, 394, 11), // "channelType"
QT_MOC_LITERAL(35, 406, 9), // "nicknames"
QT_MOC_LITERAL(36, 416, 24), // "getNamesReplyEndReceived"
QT_MOC_LITERAL(37, 441, 24), // "errNicknameInUseReceived"
QT_MOC_LITERAL(38, 466, 13), // "errorNickname"
QT_MOC_LITERAL(39, 480, 12), // "disconnected"
QT_MOC_LITERAL(40, 493, 8), // "readData"
QT_MOC_LITERAL(41, 502, 18), // "handleDisconnected"
QT_MOC_LITERAL(42, 521, 11), // "handleError"
QT_MOC_LITERAL(43, 533, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(44, 562, 11), // "socketError"
QT_MOC_LITERAL(45, 574, 15), // "ctcpVersionData"
QT_MOC_LITERAL(46, 590, 15), // "connectToServer"
QT_MOC_LITERAL(47, 606, 4), // "host"
QT_MOC_LITERAL(48, 611, 4), // "port"
QT_MOC_LITERAL(49, 616, 18), // "connectToSslServer"
QT_MOC_LITERAL(50, 635, 20), // "disconnectFromServer"
QT_MOC_LITERAL(51, 656, 12), // "sendPassword"
QT_MOC_LITERAL(52, 669, 8), // "password"
QT_MOC_LITERAL(53, 678, 15), // "sendSetNickname"
QT_MOC_LITERAL(54, 694, 19), // "sendUserInformation"
QT_MOC_LITERAL(55, 714, 8), // "userName"
QT_MOC_LITERAL(56, 723, 14), // "receiveWallops"
QT_MOC_LITERAL(57, 738, 11), // "isInvisible"
QT_MOC_LITERAL(58, 750, 15), // "sendSetOperator"
QT_MOC_LITERAL(59, 766, 15), // "sendSetUserMode"
QT_MOC_LITERAL(60, 782, 8), // "UserMode"
QT_MOC_LITERAL(61, 791, 4), // "mode"
QT_MOC_LITERAL(62, 796, 19), // "sendRegisterService"
QT_MOC_LITERAL(63, 816, 12), // "distribution"
QT_MOC_LITERAL(64, 829, 4), // "info"
QT_MOC_LITERAL(65, 834, 8), // "sendQuit"
QT_MOC_LITERAL(66, 843, 14), // "sendServerQuit"
QT_MOC_LITERAL(67, 858, 6), // "server"
QT_MOC_LITERAL(68, 865, 8), // "sendJoin"
QT_MOC_LITERAL(69, 874, 8), // "channels"
QT_MOC_LITERAL(70, 883, 8), // "sendPart"
QT_MOC_LITERAL(71, 892, 11), // "sendPartAll"
QT_MOC_LITERAL(72, 904, 18), // "sendSetChannelMode"
QT_MOC_LITERAL(73, 923, 12), // "sendSetTopic"
QT_MOC_LITERAL(74, 936, 12), // "sendGetTopic"
QT_MOC_LITERAL(75, 949, 12), // "sendGetNames"
QT_MOC_LITERAL(76, 962, 16), // "sendListChannels"
QT_MOC_LITERAL(77, 979, 15), // "sendListChannel"
QT_MOC_LITERAL(78, 995, 10), // "sendInvite"
QT_MOC_LITERAL(79, 1006, 8), // "sendKick"
QT_MOC_LITERAL(80, 1015, 9), // "usernames"
QT_MOC_LITERAL(81, 1025, 11), // "sendMessage"
QT_MOC_LITERAL(82, 1037, 11), // "sendGetMOTD"
QT_MOC_LITERAL(83, 1049, 12), // "sendGetUsers"
QT_MOC_LITERAL(84, 1062, 14), // "sendGetVersion"
QT_MOC_LITERAL(85, 1077, 12), // "sendGetStats"
QT_MOC_LITERAL(86, 1090, 5), // "query"
QT_MOC_LITERAL(87, 1096, 11), // "sendGetTime"
QT_MOC_LITERAL(88, 1108, 11), // "sendConnect"
QT_MOC_LITERAL(89, 1120, 12), // "targetServer"
QT_MOC_LITERAL(90, 1133, 12), // "remoteServer"
QT_MOC_LITERAL(91, 1146, 9), // "sendTrace"
QT_MOC_LITERAL(92, 1156, 12), // "sendGetAdmin"
QT_MOC_LITERAL(93, 1169, 17), // "sendGetServerInfo"
QT_MOC_LITERAL(94, 1187, 7), // "sendWho"
QT_MOC_LITERAL(95, 1195, 4), // "mask"
QT_MOC_LITERAL(96, 1200, 13), // "operatorsOnly"
QT_MOC_LITERAL(97, 1214, 15), // "sendGetUserInfo"
QT_MOC_LITERAL(98, 1230, 5), // "masks"
QT_MOC_LITERAL(99, 1236, 8), // "sendKill"
QT_MOC_LITERAL(100, 1245, 8), // "sendPing"
QT_MOC_LITERAL(101, 1254, 8), // "sendPong"
QT_MOC_LITERAL(102, 1263, 23), // "sendReloadConfiguration"
QT_MOC_LITERAL(103, 1287, 18), // "sendShutdownServer"
QT_MOC_LITERAL(104, 1306, 17), // "sendRestartServer"
QT_MOC_LITERAL(105, 1324, 7), // "getUser"
QT_MOC_LITERAL(106, 1332, 8), // "IRCUser*"
QT_MOC_LITERAL(107, 1341, 14), // "changeNickname"
QT_MOC_LITERAL(108, 1356, 11), // "oldNickname"
QT_MOC_LITERAL(109, 1368, 11) // "newNickname"

    },
    "IRCClient\0commandReceived\0\0prefix\0"
    "command\0params\0commandSent\0welcomeReceived\0"
    "nickname\0message\0nicknameChangedReceived\0"
    "joinReceived\0channel\0quitReceived\0"
    "partReceived\0setTopicReceived\0topic\0"
    "messageReceived\0target\0text\0canAutoReply\0"
    "killReceived\0comment\0pingReceived\0"
    "server1\0server2\0pongReceived\0errorReceived\0"
    "userInfoReceived\0username\0hostname\0"
    "realName\0getTopicReplyReceived\0"
    "getNamesReplyReceived\0channelType\0"
    "nicknames\0getNamesReplyEndReceived\0"
    "errNicknameInUseReceived\0errorNickname\0"
    "disconnected\0readData\0handleDisconnected\0"
    "handleError\0QAbstractSocket::SocketError\0"
    "socketError\0ctcpVersionData\0connectToServer\0"
    "host\0port\0connectToSslServer\0"
    "disconnectFromServer\0sendPassword\0"
    "password\0sendSetNickname\0sendUserInformation\0"
    "userName\0receiveWallops\0isInvisible\0"
    "sendSetOperator\0sendSetUserMode\0"
    "UserMode\0mode\0sendRegisterService\0"
    "distribution\0info\0sendQuit\0sendServerQuit\0"
    "server\0sendJoin\0channels\0sendPart\0"
    "sendPartAll\0sendSetChannelMode\0"
    "sendSetTopic\0sendGetTopic\0sendGetNames\0"
    "sendListChannels\0sendListChannel\0"
    "sendInvite\0sendKick\0usernames\0sendMessage\0"
    "sendGetMOTD\0sendGetUsers\0sendGetVersion\0"
    "sendGetStats\0query\0sendGetTime\0"
    "sendConnect\0targetServer\0remoteServer\0"
    "sendTrace\0sendGetAdmin\0sendGetServerInfo\0"
    "sendWho\0mask\0operatorsOnly\0sendGetUserInfo\0"
    "masks\0sendKill\0sendPing\0sendPong\0"
    "sendReloadConfiguration\0sendShutdownServer\0"
    "sendRestartServer\0getUser\0IRCUser*\0"
    "changeNickname\0oldNickname\0newNickname"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_IRCClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      95,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      19,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,  489,    2, 0x06 /* Public */,
       6,    2,  496,    2, 0x06 /* Public */,
       7,    3,  501,    2, 0x06 /* Public */,
      10,    2,  508,    2, 0x06 /* Public */,
      11,    2,  513,    2, 0x06 /* Public */,
      13,    2,  518,    2, 0x06 /* Public */,
      14,    3,  523,    2, 0x06 /* Public */,
      15,    3,  530,    2, 0x06 /* Public */,
      17,    4,  537,    2, 0x06 /* Public */,
      21,    3,  546,    2, 0x06 /* Public */,
      23,    3,  553,    2, 0x06 /* Public */,
      26,    3,  560,    2, 0x06 /* Public */,
      27,    2,  567,    2, 0x06 /* Public */,
      28,    5,  572,    2, 0x06 /* Public */,
      32,    3,  583,    2, 0x06 /* Public */,
      33,    5,  590,    2, 0x06 /* Public */,
      36,    4,  601,    2, 0x06 /* Public */,
      37,    4,  610,    2, 0x06 /* Public */,
      39,    0,  619,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      40,    0,  620,    2, 0x08 /* Private */,
      41,    0,  621,    2, 0x08 /* Private */,
      42,    1,  622,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      45,    0,  625,    2, 0x02 /* Public */,
      46,    2,  626,    2, 0x02 /* Public */,
      49,    2,  631,    2, 0x02 /* Public */,
      50,    0,  636,    2, 0x02 /* Public */,
      51,    1,  637,    2, 0x02 /* Public */,
      53,    1,  640,    2, 0x02 /* Public */,
      54,    4,  643,    2, 0x02 /* Public */,
      54,    2,  652,    2, 0x02 /* Public */,
      58,    2,  657,    2, 0x02 /* Public */,
      59,    2,  662,    2, 0x02 /* Public */,
      62,    3,  667,    2, 0x02 /* Public */,
      65,    1,  674,    2, 0x02 /* Public */,
      65,    0,  677,    2, 0x02 /* Public */,
      66,    2,  678,    2, 0x02 /* Public */,
      68,    1,  683,    2, 0x02 /* Public */,
      68,    1,  686,    2, 0x02 /* Public */,
      70,    2,  689,    2, 0x02 /* Public */,
      70,    2,  694,    2, 0x02 /* Public */,
      70,    1,  699,    2, 0x02 /* Public */,
      70,    1,  702,    2, 0x02 /* Public */,
      71,    0,  705,    2, 0x02 /* Public */,
      72,    1,  706,    2, 0x02 /* Public */,
      73,    2,  709,    2, 0x02 /* Public */,
      74,    1,  714,    2, 0x02 /* Public */,
      75,    2,  717,    2, 0x02 /* Public */,
      75,    1,  722,    2, 0x02 /* Public */,
      76,    2,  725,    2, 0x02 /* Public */,
      76,    1,  730,    2, 0x02 /* Public */,
      76,    0,  733,    2, 0x02 /* Public */,
      77,    2,  734,    2, 0x02 /* Public */,
      77,    1,  739,    2, 0x02 /* Public */,
      78,    2,  742,    2, 0x02 /* Public */,
      79,    3,  747,    2, 0x02 /* Public */,
      79,    2,  754,    2, 0x02 /* Public */,
      79,    3,  759,    2, 0x02 /* Public */,
      79,    2,  766,    2, 0x02 /* Public */,
      81,    3,  771,    2, 0x02 /* Public */,
      81,    2,  778,    2, 0x02 /* Public */,
      82,    1,  783,    2, 0x02 /* Public */,
      82,    0,  786,    2, 0x02 /* Public */,
      83,    1,  787,    2, 0x02 /* Public */,
      83,    0,  790,    2, 0x02 /* Public */,
      84,    1,  791,    2, 0x02 /* Public */,
      84,    0,  794,    2, 0x02 /* Public */,
      85,    2,  795,    2, 0x02 /* Public */,
      85,    1,  800,    2, 0x02 /* Public */,
      85,    0,  803,    2, 0x02 /* Public */,
      87,    1,  804,    2, 0x02 /* Public */,
      87,    0,  807,    2, 0x02 /* Public */,
      88,    3,  808,    2, 0x02 /* Public */,
      88,    2,  815,    2, 0x02 /* Public */,
      91,    1,  820,    2, 0x02 /* Public */,
      91,    0,  823,    2, 0x02 /* Public */,
      92,    1,  824,    2, 0x02 /* Public */,
      92,    0,  827,    2, 0x02 /* Public */,
      93,    1,  828,    2, 0x02 /* Public */,
      93,    0,  831,    2, 0x02 /* Public */,
      94,    2,  832,    2, 0x02 /* Public */,
      94,    1,  837,    2, 0x02 /* Public */,
      94,    0,  840,    2, 0x02 /* Public */,
      97,    2,  841,    2, 0x02 /* Public */,
      97,    1,  846,    2, 0x02 /* Public */,
      97,    1,  849,    2, 0x02 /* Public */,
      99,    2,  852,    2, 0x02 /* Public */,
     100,    2,  857,    2, 0x02 /* Public */,
     100,    1,  862,    2, 0x02 /* Public */,
     101,    2,  865,    2, 0x02 /* Public */,
     101,    1,  870,    2, 0x02 /* Public */,
     102,    0,  873,    2, 0x02 /* Public */,
     103,    0,  874,    2, 0x02 /* Public */,
     104,    0,  875,    2, 0x02 /* Public */,
     105,    1,  876,    2, 0x02 /* Public */,
     107,    2,  879,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QStringList,    3,    4,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QStringList,    4,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    8,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    8,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,   12,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   12,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   12,   16,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::Bool,    3,   18,   19,   20,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    8,   22,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   24,   25,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   24,   25,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    8,   29,   30,   31,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   12,   16,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QStringList,    3,    8,   34,   12,   35,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    8,   12,   22,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    8,   38,   22,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 43,   44,

 // methods: parameters
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort,   47,   48,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort,   47,   48,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   52,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool, QMetaType::Bool, QMetaType::QString,   55,   56,   57,   31,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   55,   31,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   55,   52,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 60,    8,   61,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    8,   63,   64,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   67,   22,
    QMetaType::Void, QMetaType::QStringList,   69,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString,   69,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,    9,
    QMetaType::Void, QMetaType::QStringList,   69,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   16,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString,   69,   18,
    QMetaType::Void, QMetaType::QStringList,   69,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString,   69,   18,
    QMetaType::Void, QMetaType::QStringList,   69,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   18,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    8,   12,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QStringList, QMetaType::QString,   69,   80,   22,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QStringList,   69,   80,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,   12,   29,   22,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   29,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Bool,   18,   19,   20,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   18,   19,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   86,   18,
    QMetaType::Void, QMetaType::QString,   86,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UInt, QMetaType::QString,   89,   48,   90,
    QMetaType::Void, QMetaType::QString, QMetaType::UInt,   89,   48,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   95,   96,
    QMetaType::Void, QMetaType::QString,   95,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QStringList,   18,   98,
    QMetaType::Void, QMetaType::QStringList,   98,
    QMetaType::Void, QMetaType::QString,   95,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    8,    4,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   24,   25,
    QMetaType::Void, QMetaType::QString,   67,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   24,   25,
    QMetaType::Void, QMetaType::QString,   67,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 106, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,  108,  109,

       0        // eod
};

void IRCClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        IRCClient *_t = static_cast<IRCClient *>(_o);
        switch (_id) {
        case 0: _t->commandReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QStringList(*)>(_a[3]))); break;
        case 1: _t->commandSent((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2]))); break;
        case 2: _t->welcomeReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 3: _t->nicknameChangedReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 4: _t->joinReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 5: _t->quitReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 6: _t->partReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 7: _t->setTopicReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 8: _t->messageReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 9: _t->killReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 10: _t->pingReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 11: _t->pongReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 12: _t->errorReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 13: _t->userInfoReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5]))); break;
        case 14: _t->getTopicReplyReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 15: _t->getNamesReplyReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QStringList(*)>(_a[5]))); break;
        case 16: _t->getNamesReplyEndReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 17: _t->errNicknameInUseReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 18: _t->disconnected(); break;
        case 19: _t->readData(); break;
        case 20: _t->handleDisconnected(); break;
        case 21: _t->handleError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 22: { QString _r = _t->ctcpVersionData();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 23: _t->connectToServer((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 24: _t->connectToSslServer((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 25: _t->disconnectFromServer(); break;
        case 26: _t->sendPassword((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->sendSetNickname((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 28: _t->sendUserInformation((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 29: _t->sendUserInformation((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 30: _t->sendSetOperator((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 31: _t->sendSetUserMode((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< UserMode(*)>(_a[2]))); break;
        case 32: _t->sendRegisterService((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 33: _t->sendQuit((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 34: _t->sendQuit(); break;
        case 35: _t->sendServerQuit((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 36: _t->sendJoin((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 37: _t->sendJoin((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 38: _t->sendPart((*reinterpret_cast< const QStringList(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 39: _t->sendPart((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 40: _t->sendPart((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 41: _t->sendPart((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 42: _t->sendPartAll(); break;
        case 43: _t->sendSetChannelMode((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 44: _t->sendSetTopic((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 45: _t->sendGetTopic((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 46: _t->sendGetNames((*reinterpret_cast< const QStringList(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 47: _t->sendGetNames((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 48: _t->sendListChannels((*reinterpret_cast< const QStringList(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 49: _t->sendListChannels((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 50: _t->sendListChannels(); break;
        case 51: _t->sendListChannel((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 52: _t->sendListChannel((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 53: _t->sendInvite((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 54: _t->sendKick((*reinterpret_cast< const QStringList(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 55: _t->sendKick((*reinterpret_cast< const QStringList(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2]))); break;
        case 56: _t->sendKick((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 57: _t->sendKick((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 58: _t->sendMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 59: _t->sendMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 60: _t->sendGetMOTD((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 61: _t->sendGetMOTD(); break;
        case 62: _t->sendGetUsers((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 63: _t->sendGetUsers(); break;
        case 64: _t->sendGetVersion((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 65: _t->sendGetVersion(); break;
        case 66: _t->sendGetStats((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 67: _t->sendGetStats((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 68: _t->sendGetStats(); break;
        case 69: _t->sendGetTime((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 70: _t->sendGetTime(); break;
        case 71: _t->sendConnect((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 72: _t->sendConnect((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2]))); break;
        case 73: _t->sendTrace((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 74: _t->sendTrace(); break;
        case 75: _t->sendGetAdmin((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 76: _t->sendGetAdmin(); break;
        case 77: _t->sendGetServerInfo((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 78: _t->sendGetServerInfo(); break;
        case 79: _t->sendWho((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 80: _t->sendWho((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 81: _t->sendWho(); break;
        case 82: _t->sendGetUserInfo((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2]))); break;
        case 83: _t->sendGetUserInfo((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 84: _t->sendGetUserInfo((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 85: _t->sendKill((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 86: _t->sendPing((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 87: _t->sendPing((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 88: _t->sendPong((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 89: _t->sendPong((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 90: _t->sendReloadConfiguration(); break;
        case 91: _t->sendShutdownServer(); break;
        case 92: _t->sendRestartServer(); break;
        case 93: { IRCUser* _r = _t->getUser((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< IRCUser**>(_a[0]) = _r; }  break;
        case 94: _t->changeNickname((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QStringList & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::commandReceived)) {
                *result = 0;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QStringList & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::commandSent)) {
                *result = 1;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::welcomeReceived)) {
                *result = 2;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::nicknameChangedReceived)) {
                *result = 3;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::joinReceived)) {
                *result = 4;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::quitReceived)) {
                *result = 5;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::partReceived)) {
                *result = 6;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::setTopicReceived)) {
                *result = 7;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::messageReceived)) {
                *result = 8;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::killReceived)) {
                *result = 9;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::pingReceived)) {
                *result = 10;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::pongReceived)) {
                *result = 11;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::errorReceived)) {
                *result = 12;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::userInfoReceived)) {
                *result = 13;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::getTopicReplyReceived)) {
                *result = 14;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & , const QString & , const QStringList & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::getNamesReplyReceived)) {
                *result = 15;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::getNamesReplyEndReceived)) {
                *result = 16;
            }
        }
        {
            typedef void (IRCClient::*_t)(const QString & , const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::errNicknameInUseReceived)) {
                *result = 17;
            }
        }
        {
            typedef void (IRCClient::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&IRCClient::disconnected)) {
                *result = 18;
            }
        }
    }
}

const QMetaObject IRCClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_IRCClient.data,
      qt_meta_data_IRCClient,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *IRCClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *IRCClient::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_IRCClient.stringdata))
        return static_cast<void*>(const_cast< IRCClient*>(this));
    return QObject::qt_metacast(_clname);
}

int IRCClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 95)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 95;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 95)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 95;
    }
    return _id;
}

// SIGNAL 0
void IRCClient::commandReceived(const QString & _t1, const QString & _t2, const QStringList & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void IRCClient::commandSent(const QString & _t1, const QStringList & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void IRCClient::welcomeReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void IRCClient::nicknameChangedReceived(const QString & _t1, const QString & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void IRCClient::joinReceived(const QString & _t1, const QString & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void IRCClient::quitReceived(const QString & _t1, const QString & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void IRCClient::partReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void IRCClient::setTopicReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void IRCClient::messageReceived(const QString & _t1, const QString & _t2, const QString & _t3, bool _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void IRCClient::killReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void IRCClient::pingReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void IRCClient::pongReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void IRCClient::errorReceived(const QString & _t1, const QString & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void IRCClient::userInfoReceived(const QString & _t1, const QString & _t2, const QString & _t3, const QString & _t4, const QString & _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void IRCClient::getTopicReplyReceived(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void IRCClient::getNamesReplyReceived(const QString & _t1, const QString & _t2, const QString & _t3, const QString & _t4, const QStringList & _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void IRCClient::getNamesReplyEndReceived(const QString & _t1, const QString & _t2, const QString & _t3, const QString & _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void IRCClient::errNicknameInUseReceived(const QString & _t1, const QString & _t2, const QString & _t3, const QString & _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void IRCClient::disconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
